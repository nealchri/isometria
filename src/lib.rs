pub mod isogrid {
    pub struct Isogrid {
        x_res: usize,
        y_res: usize,
        data: Vec<usize>,
    }
    fn offset(x_res: usize, x: usize, y: usize) -> usize {
         y*x_res + x
    }   
    impl Isogrid {
        pub fn new(x: usize, y: usize) -> Isogrid {
            Isogrid {
                x_res: x,
                y_res: y,
                data: Vec::with_capacity(x * y),
            }
        }
        pub fn fill(&mut self, num: usize) {
            while self.data.len() < self.x_res * self.y_res {
                self.data.push(num);
            }
        }
        pub fn val(&mut self, x: usize, y: usize) -> &mut usize {
            let val = self.data.get_mut(offset(self.x_res,x,y));
            match val {
                Some (x) => x,
                None => panic!()
            }
        }
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let mut t = isogrid::Isogrid::new(10, 10);
        t.fill(0);
        *(t.val(2,2)) = 5;
        assert_eq!(*(t.val(2,2)), 5);
    }
}
